//Fetching elements and setting variables
let moneyFromWork = document.getElementById("moneyFromWork")
let bankBalance = document.getElementById("bankBalance")
let price1 = document.getElementById("laptop1Price")
let name1 = document.getElementById("laptop1Name")
let money = 0
let bankTotal = 0
let counter = 0

//Function that "gives" the user money when they click on the "Work" button. 
//It increments with 100 SEK every time.
function work() {
	console.log("clicked the workbtn")
	money += 100
	moneyFromWork.innerHTML = money+'kr'
}

//Function to deposit money to the bank. It adds the money from your pay to the balance, 
//and sets your "pay" back to 0. 
function deposit() {
	console.log(typeof money)
	console.log("clicked the depositbtn")
	bankTotal += money
	console.log(money, bankTotal)
	money = 0
	bankBalance.innerHTML = bankTotal + 'kr'
	moneyFromWork.innerHTML = money+'kr'
}

//Function to get a loan. Starts with showing a prompt for you to enter what you want to loan.
//If what you write is either null, empty och not a number, an alert shows you that you need a valid number.
//else if checks to see that you can only loan up to dubble your deposit.
//else is there if you enter a number that is too high. 
//Using a counter to check if the button have been pushed more than once, if it has you can't loan again. 
//This counter gets reset to 0 in the buyLaptop()
function getloan() {
	let loan = 0
	let prom = prompt('Please enter the amount of money you want to loan.')

	if(prom == null || prom == ' ' || isNaN(prom)) {
		alert('You need to write a valid number.')
	}
	else if(counter == 0) {

		if(parseInt(prom) <= (bankTotal*2)) {
			loan = parseInt(prom)
			bankTotal += loan
			bankBalance.innerHTML = bankTotal +'kr'
			alert('You get a loan! What a happy day!')
			counter++
		}
		else {
			alert('You are not allowed to get a loan of this size, you need more money in your deposit first.')
		}
	}
	else {
		alert('You have already gotten a loan, please purchase a computer before getting a new loan. Or get to work..')
	}
}

//Function for buying laptop. price and name are taken from which button is clicked. The buttons have different id's.
//It checks if you have oney in the bank, and if you do, the name shows up under "Purchased computers".
//And banktotal gets reduced by the price of the computer. 
//The counter that checks if you are allowed to loan money, gets reset to 0.
//If you don't have money, you get an alert that says so.
function buyLaptop(price, name) {
	console.log('clicked the buyLaptopbtn')

	if(bankTotal >= price) {
		console.log('You have enough money to buy this')
		let computer = document.getElementById("purchasediv")
		let computername = document.createElement("p")
		computername.innerHTML = name
		computer.appendChild(computername)
		bankTotal -= price
		bankBalance.innerHTML = bankTotal 
		counter = 0
	}
	else {
		alert('You do not have enough money for this laptop. Try another one or make more money.')
	}

}